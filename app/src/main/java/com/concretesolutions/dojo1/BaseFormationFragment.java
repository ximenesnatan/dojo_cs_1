package com.concretesolutions.dojo1;

import android.support.v4.app.Fragment;

import com.concretesolutions.dojo1.ui.views.PlayerCircle;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Natan on 17/04/2016.
 */
@EFragment
public class BaseFormationFragment extends Fragment{
    @ViewById(R.id.p1)
    public PlayerCircle p1;
    @ViewById(R.id.p2)
    public PlayerCircle p2;
    @ViewById(R.id.p3)
    public PlayerCircle p3;
    @ViewById(R.id.p4)
    public PlayerCircle p4;
    @ViewById(R.id.p5)
    public PlayerCircle p5;
    @ViewById(R.id.p6)
    public PlayerCircle p6;
    @ViewById(R.id.p7)
    public PlayerCircle p7;
    @ViewById(R.id.p8)
    public PlayerCircle p8;
    @ViewById(R.id.p9)
    public PlayerCircle p9;
    @ViewById(R.id.p10)
    public PlayerCircle p10;
    @ViewById(R.id.p11)
    public PlayerCircle p11;
}
