package com.concretesolutions.dojo1;

import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @ViewById
    Spinner spinner;

    @ViewById
    RelativeLayout field;

    @ViewById
    RelativeLayout formationContainer;

    @AfterViews
    void afterViews() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.formations_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(formationContainer.getId(), Formation442Fragment_.builder().build());
        transaction.commit();

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                Formation442Fragment formation442Fragment = Formation442Fragment_.builder().build();
                replaceFragmentWithAnimation(formation442Fragment);
                break;

            case 1:
                Formation433Fragment formation433Fragment = Formation433Fragment_.builder().build();
                replaceFragmentWithAnimation(formation433Fragment);
                break;
        }


    }

    public void replaceFragmentWithAnimation(BaseFormationFragment baseFormationFragment) {
        BaseFormationFragment currentFrag = (BaseFormationFragment) getSupportFragmentManager().findFragmentById(R.id.formationContainer);

        if (currentFrag != null) {

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.formationContainer, baseFormationFragment)
                    .addSharedElement(currentFrag.p9, "player9")
                    .addSharedElement(currentFrag.p10, "player10")
                    .addSharedElement(currentFrag.p11, "player11")
                    .addSharedElement(currentFrag.p7, "player7")
                    .addSharedElement(currentFrag.p8, "player8")
                    .addSharedElement(currentFrag.p5, "player5");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                baseFormationFragment.setSharedElementReturnTransition(new ChangeBounds());
                baseFormationFragment.setSharedElementEnterTransition(new ChangeBounds());
            }
            transaction.commit();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
